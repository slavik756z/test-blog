<?php

// - получения текста статьи
$article = $pdo->query('select * from articles where id = ' . $_GET['id'])->fetch(PDO::FETCH_ASSOC);

// - получить комментарии
$comments = $pdo->query('select * from comments where article_id = ' . $_GET['id'])->fetchAll(PDO::FETCH_ASSOC);

?>

<!--статья-->
<div class="col-md-12" >
    <div class="article-text">
    <?= $article['text']?>
    <div>
        <?= 'Автор: ' . $article['author'] ?>
        <span class='date'><?= date("d.m.y H:i" ,$article['created_at']) ?></span>
    </div>
    </div>
</div>

<div id="comments"><h3>Комментарии</h3></div>

<!--комментарии-->
<div class="col-md-12" >
    <?php
    foreach ($comments  as $comment ){
        ?>
        <div class="comment-block" >
            <p>
                <?= $comment['text'] ?>
            </p>
            <div class="cart-block-bottom">
                <?= 'Автор: ' . $comment['comment_author'] ?>
                <span class="date"><?=  date("d.m.y H:i" ,$comment['created_at']) ?></span>
            </div>
        </div>
        <?php
    }?>
</div>

<!--форма добавления комментария-->
<div class="add-form">
    <div class="col-md-8 col-md-offset-2" >
        <?= $_COOKIE['message'] ?>
        <form class="button-block col-md-6 col-md-offset-3" action="add-comment" method="post">
            <input type="hidden" name="article_id" value="<?= $_GET['id'] ?>">
            <div class="form-group">
                <label for="exampleInputPassword1">Имя Автора комментария</label>
                <input class="form-control" type="text" name="comment_author" placeholder="Имя">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Текст комментария</label>
                <textarea class="form-control" name="comment_text" cols="40" rows="6"></textarea>
            </div>
            <input class="btn btn-success" type="submit">
        </form>
    </div>
</div>