<!DOCTYPE html>
<html>
<title>Test</title>

<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/main_style.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<link href="/owl.carousel.2.0.0-beta.2.4/assets/owl.carousel.css" rel="stylesheet">
<script src="/owl.carousel.2.0.0-beta.2.4/owl.carousel.min.js"></script>

    <body >

        <h1>Test Blog</h1>
        <div class="button-block col-md-6 col-md-offset-3" >
            <a href="/"><span class="block">Home</span></a>
        </div>

        <!--блок контента-->
        <div class="button-block col-md-8 col-md-offset-2" >
            <?php include_once($content); ?>
        </div>
    </body>
</html>

<script>
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    })
</script>