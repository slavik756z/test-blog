<?php

// - все статьи отсортированые по убыванию даты и обрезаным текстом статьи
$articles = $pdo->query('select comments,SUBSTR(text,1,100) as text,created_at,id,author from articles a left join (select article_id,count(*) as comments from comments group by article_id) m_t on m_t.article_id  = a.id order by created_at desc')->fetchAll(PDO::FETCH_ASSOC);

// - топ 5 самых комментируемых записей
$articles_famous = $pdo->query('select comments,SUBSTR(text,1,100) as text,created_at,id,author from articles a left join (select article_id,count(*) as comments from comments group by article_id) m_t on m_t.article_id  = a.id order by comments desc limit 5')->fetchAll(PDO::FETCH_ASSOC);

?>

<div>
<!--    слайдер с 5 самых комментируемых записей-->
    <div class="owl-carousel">
        <?php
        foreach ($articles_famous as $article){
            ?>
            <div class="item" >
                <div class="pre-block">
                    <div class="pre-title">
                        <?= $article['text'] . '... '; ?>
                    </div>
                    <div class="pre-author" >
                        <?= 'Автор: ' . $article['author'] ?>
                        <span class="comments"><?= 'Комментариев: ' . ($article['comments'] != null ? $article['comments'] : 0) ?></span>
                        <span class="date"><?= date("d.m.y H:i" ,$article['created_at']) ?></span>
                    </div>
                </div>
            </div>
            <?php
        }?>
    </div>

<!--    добавление новой статьи-->
    <div class="add-form">
        <div class="col-md-10 col-md-offset-1" >
            <?= $_COOKIE['message'] ?>
            <form class="button-block col-md-10 col-md-offset-1" action="add" method="post">
                <div class="form-group">
                    <label for="exampleInputPassword1">Имя Автора</label>
                    <input class="form-control" type="text" name="author" placeholder="Имя">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Текст статьи</label>
                    <textarea class="form-control" name="text" cols="90" rows="6"></textarea>
                </div>
                <input class="btn btn-success" type="submit">
            </form>
        </div>
    </div>
<!--    вывод списка статей-->
    <div class="main-block col-md-10 col-md-offset-1">
        <!--<div class="main-block col-md-10 col-md-offset-1" ng-repeat="item in array">-->

        <?php
            foreach ($articles as $article){
            ?>
                <div class="cart-block" >

                    <p>
                        <a href="<?= 'article?id=' . $article['id'] ?>"><?= $article['text'] ?></a>
                    </p>
                    <div class="cart-block-bottom">
                        <?= 'Автор: ' . $article['author'] ?>
                        <span class="comments"><?= 'Комментариев: ' . ($article['comments'] != null ? $article['comments'] : 0) ?></span>
                        <span class="date"><?=  date("d.m.y H:i" ,$article['created_at']) ?></span>
                    </div>
                </div>
           <?php
        }?>
    </div>
</div>