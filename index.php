<?php

// - подключение базі
include_once('inc/db.php');

// - парсинг url
$path = explode('?',$_SERVER['REQUEST_URI']);
$path = explode('/',substr($path[0],1));

// - роутинг
if(count($path) < 2){
    switch ($path[0]){
        case 'add': // - добавление статьи
            include_once('add_article.php');
            break;
        case 'add-comment'; // - добавление коммента
            include_once('add_comment.php');
            break;
        case 'article': // - отображение страници статьи
            $content = 'show-article.php';
            include_once('templates/main-template.php');
            break;
        default: // - главная страница
            if(isset($_COOKIE['message'])) {
                setcookie("message", '');
            }
            $content = 'get_articles.php';
            include_once('templates/main-template.php');
            break;
    }
}else{
    include_once('templates/404.html');
}