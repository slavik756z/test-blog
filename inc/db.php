<?php

// - найстройки базы
$conf = [
    'dsn' => 'mysql:host=127.0.0.1;dbname=test_db',
    'username' => 'root',
    'password' => '',
];

try {
    $pdo = new PDO($conf['dsn'], $conf['username'], $conf['password']);
} catch (PDOException $e) {
    die('Подключение не удалось: ' . $e->getMessage());
}